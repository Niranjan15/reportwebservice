package com.ge.report.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ge.report.common.RespObject;
import com.ge.report.service.interfaces.ReportService;

@RestController
public class ReportController {

	private static final String	EXCEPTION	= "Exception";

	private static final Logger	OUT			= LoggerFactory.getLogger(ReportController.class);

	@Autowired
	private ReportService		reportService;

	@RequestMapping(value = "/line", method = RequestMethod.GET)
	public RespObject lineChart() {
		RespObject data = new RespObject();
		try {
			data.setResult(reportService.lineChart());
			data.setStatus("success");
		} catch (Exception e) {
			OUT.error(EXCEPTION, e);
			data.setStatus("error");
		}
		return data;
	}

	@RequestMapping(value = "/area", method = RequestMethod.GET)
	public RespObject areaChart() {
		RespObject data = new RespObject();
		try {
			data.setResult(reportService.areaChart());
			data.setStatus("success");
		} catch (Exception e) {
			OUT.error(EXCEPTION, e);
			data.setStatus("error");
		}
		return data;
	}

	@RequestMapping(value = "/column", method = RequestMethod.GET)
	public RespObject columnChart() {
		RespObject data = new RespObject();
		try {
			data.setResult(reportService.columnChart());
			data.setStatus("success");
		} catch (Exception e) {
			OUT.error(EXCEPTION, e);
			data.setStatus("error");
		}
		return data;
	}

	@RequestMapping(value = "/bar", method = RequestMethod.GET)
	public RespObject barChart() {
		RespObject data = new RespObject();
		try {
			data.setResult(reportService.barChart());
			data.setStatus("success");
		} catch (Exception e) {
			OUT.error(EXCEPTION, e);
			data.setStatus("error");
		}
		return data;
	}

	@RequestMapping(value = "/pie", method = RequestMethod.GET)
	public RespObject pieChart() {
		RespObject data = new RespObject();
		try {
			data.setResult(reportService.pieChart());
			data.setStatus("success");
		} catch (Exception e) {
			OUT.error(EXCEPTION, e);
			data.setStatus("error");
		}
		return data;
	}

	@RequestMapping(value = "/bubble", method = RequestMethod.GET)
	public RespObject bubbleChart() {
		RespObject data = new RespObject();
		try {
			data.setResult(reportService.bubbleChart());
			data.setStatus("success");
		} catch (Exception e) {
			OUT.error(EXCEPTION, e);
			data.setStatus("error");
		}
		return data;
	}

	@RequestMapping(value = "/combo", method = RequestMethod.GET)
	public RespObject combinationChart() {
		RespObject data = new RespObject();
		try {
			data.setResult(reportService.combinationChart());
			data.setStatus("success");
		} catch (Exception e) {
			OUT.error(EXCEPTION, e);
			data.setStatus("error");
		}
		return data;
	}

	@RequestMapping(value = "/threed", method = RequestMethod.GET)
	public RespObject threeDChart() {
		RespObject data = new RespObject();
		try {
			data.setResult(reportService.threeDChart());
			data.setStatus("success");
		} catch (Exception e) {
			OUT.error(EXCEPTION, e);
			data.setStatus("error");
		}
		return data;
	}

	@RequestMapping(value = "/gauges", method = RequestMethod.GET)
	public RespObject gaugesChart() {
		RespObject data = new RespObject();
		try {
			data.setResult(reportService.gaugesChart());
			data.setStatus("success");
		} catch (Exception e) {
			OUT.error(EXCEPTION, e);
			data.setStatus("error");
		}
		return data;
	}
}
