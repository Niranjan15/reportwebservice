package com.ge.report.service.impls;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.ge.report.service.interfaces.ReportService;

@Service
public class ReportServiceImpl implements ReportService {

	private static final Logger	OUT		= LoggerFactory.getLogger(ReportServiceImpl.class);

	private static final String	AREA	= "highchart/areachart.txt";
	private static final String	BAR		= "highchart/barchart.txt";
	private static final String	BUBBLE	= "highchart/bubblechart.txt";
	private static final String	COLUMN	= "highchart/columnchart.txt";
	private static final String	COMBO	= "highchart/combochart.txt";
	private static final String	GAUGES	= "highchart/gaugeschart.txt";
	private static final String	LINE	= "highchart/linechart.txt";
	private static final String	PIE		= "highchart/piechart.txt";
	private static final String	THREED	= "highchart/threedchart.txt";

	@Override
	public String lineChart() {
		return readContent(LINE);
	}

	@Override
	public String areaChart() {
		return readContent(AREA);
	}

	@Override
	public String columnChart() {
		return readContent(COLUMN);
	}

	@Override
	public String barChart() {
		return readContent(BAR);
	}

	@Override
	public String pieChart() {
		return readContent(PIE);
	}

	@Override
	public String bubbleChart() {
		return readContent(BUBBLE);
	}

	@Override
	public String combinationChart() {
		return readContent(COMBO);
	}

	@Override
	public String threeDChart() {
		return readContent(THREED);
	}

	@Override
	public String gaugesChart() {
		return readContent(GAUGES);
	}

	private String readContent(String filePath) {
		StringBuilder fileContent = new StringBuilder();
		try (BufferedReader resourceAsStream = new BufferedReader(new InputStreamReader(getClass().getClassLoader().getResourceAsStream(filePath)))) {
			String line = null;
			while ((line = resourceAsStream.readLine()) != null)
				fileContent.append(line);
		} catch (Exception e) {
			OUT.error("Exception", e);
		}
		return fileContent.toString();
	}

}
