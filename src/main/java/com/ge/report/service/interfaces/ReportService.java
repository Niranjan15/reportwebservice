package com.ge.report.service.interfaces;

public interface ReportService {

	String lineChart();

	String areaChart();

	String columnChart();

	String barChart();

	String pieChart();

	String bubbleChart();

	String combinationChart();

	String threeDChart();

	String gaugesChart();
}
